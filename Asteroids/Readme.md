![FCTUNL](http://www.fct.unl.pt/sites/default/themes/fct_unl_pt_2015/images/logo.png)

# [Departamento de Informática](http://www.di.fct.unl.pt)

# Game Programming with Python - FCT

Fernando Birra (fpb@fct.unl.pt), João Costa Seco (joao.seco@fct.unl.pt) and Miguel Goulão (mgoul@fct.unl.pt)

![Python](https://www.python.org/static/community_logos/python-logo-master-v3-TM.png)

In this activity we invite you to understand the nuts and bolts of a computer game and learn some basic programming concepts along the way. We challenge you to follow a sequence of simple steps that will guide you from the basic elements of a program written in the Python programming language, to the construction of a little computer game, reusing a set of available components.

# Programming 101

Let us start by talking a bit about programming. Is that even possible, in such a short amount of time?
To build a program is, essentially, to specify the behavior of a computer, in such a way that it fulfills our purposes. We use a programming language to write this specification. With this language, we are able to express this behavior using concepts like values, variables, functions, objects and classes. In this short tutorial, we will use the Python programming language. You can download Python tools from [www.python.org](https://www.python.org).

Most modern programs and computer systems are built from pre-fabricated software components, which are used as building blocks to create new programs. These building blocks are, themselves, software components. The development of a new program often involves a combination of pre-fabricated components with new components.

Today, we will do just that. We will use an existing "game engine" -- a set of pre-fabricated components -- to build... games! The game engine is called Panda3D and may be obtained from [www.panda3d.org](http://www.panda3d.org). 

Let us start with the game Asteroids, produced by Atari in the early 80's of last century, which looked like this:

![Asteroids](resources/panda3d-Asteroids.png)

## Variables, values and operations

Python allows you to manipulate values which may represent integers, real numbers, and so on. The game you are about to build is a spaceships game. It involves some game rules, and physics. More importantly, building it is actually quite fun. Are you ready? Let's start...

The easiest way to learn about variables and their values is to play a bit with them. We will build and evaluate a few expressions using those variables.

Before we begin, we need a special program, called an "interpreter". This interpreter, in this case called **python** will allow us to run programs and play with those variables and expressions. Soon enough, we will be using the interpreter to run our games!

We will use the interpreter embedded in the Brackets editor, available in the lab. To use it, please start brackets in your computer.

Start by creating a new source code file. You may call it `example.py`, for example. The `.py` extension is important, as it denotes you are developing python code. In Brackets, go to `File`, then choose `New`. 
This should create a file called `Untitled-1`. Now choose `File` and then `Save as`. Choose a folder to store the file, name the file `example.py`, save it, and we are ready to go.

So, spaceships. 

One of the things we will need to know is where the spaceship is, in our game. As we are going to build a 2D game, we will need a couple of coordinates. We can define two variables, `x` and `y`, for the vertical and horizontal coordinates of the ship.

### example.py
~~~~
x = 10
y = 20
~~~~

These python commands will assign the variable `x` with the value 10, and the variable `y`with the value 20. These two variables may represent the spaceship's coordinates.

Spaceships are a lot more interesting if they can move, right? Let us define a velocity `v` and a direction `dir` for describing the way the spaceship is moving.

~~~~
v = 5
dir = 30
~~~~

Eventually, we will need the ship to move according to the laws of physics, for our game. For example, after a time unit goes by, we need to update the ship's position. Given the ship's current position, velocity and direction, we can compute the new position after a time unit using a bit of trigonometry. Don't worry, python already has some pretty handy functions and constants available that we can use, such as sin, cos, and the constant pi. We need to import those definitions from a library called math. Also, because the sin and cos functions work with radians, we need to define a constant (pi/180) to convert from degrees to radians. Finally, we need to update the x and y coordinates with the horizontal and vertical components of velocity v. All it takes is a couple of assignments, as before, but now with a slightly more complex expression to compute the new values of x and y.
Keep in mind that the values of `x`, `y`, `v` and `dir` have to be defined *before* they are used for updating `X` and `y`.
~~~~
from math import sin, cos, pi

DEG_TO_RAD = pi / 180
x = x + v*cos(dir*DEG_TO_RAD)
y = y + v*sin(dir*DEG_TO_RAD)
~~~~

hmmm... You may wonder what is the current value of `x` and `y`. You can ask the python interpreter to print it out for you.

~~~~
print x
print y
~~~~

Ok. Checkpoint. By now, your file should look something like this:
~~~~
from math import sin, cos, pi
DEG_TO_RAD = pi / 180
x = 10
y = 20
v = 5
dir = 30
x = x + v*cos(dir*DEG_TO_RAD)
y = y + v*sin(dir*DEG_TO_RAD)
print x
print y
~~~~

Make sure you try this. To run your program, you can simply press the green play button on the right side of the Brackets editor.

## Operations

As another time unit goes by, we will need to update the ship's position. Should we repeat those update instructions, once for each update? 
Well, you could do that, but there are better options that will even allow you to configure the desired behavior. For instance, you could vary the `dir` value, making the ship turn to the new `dir`, or the velocity of the ship.

Let us play with this idea for a while. We can create an operation, write it once, and reuse it when necessary.

~~~~
def move(dt):
    global x
    global y
    global v
    global dir

    x = x + v*cos(dir*DEG_TO_RAD)*dt
    y = y + v*sin(dir*DEG_TO_RAD)*dt
~~~~

This will allow us to move our ship repeatedly, without having to repeat those long trigonometry expressions. We can simply call the operation

~~~~
move(10)
~~~~

to see the effect of moving the ship 10 time units.

## Objects
By now, you know how to store some simple data, as well as how to represent operations. What if you could specify in one place the operations and the data those operations manipulate? As it turns out, you can. You can gather related variables and operations in a single object. In this case, your object could represent a spaceship, with its position, velocity, direction, and operations to move around. In order to create an object, we first need to specify a class describing objects of that kind, that is, with the same structure and functionality. These objects are called instances. 

Have a look at a possible definition for a class representing spaceships in our game. We have a special operation, called `__init__` which is used to build objects of the type `Ship`. This operation, which is called a constructor, receives 5 parameters: `self` (the `ship` we are building, `x` and `y`, for the position, `v` for velocity and `dir` for direction). The variables describing the state of the object are known as instance variables and are called `self.x`, `self.y`, `self.v` and `self.dir`. They receive and store the parameter values. The `move` operation becomes simpler than before, with only the updates for `self.x` and `self.y`. The operation `show` prints the values of both coordinates. Finally, `turn` updates the direction by adding the value of `deg` to the current direction. If you want to turn left, `deg` should have a negative value (e.g. -20, for turning 20 degrees to the left). Use a positive value to turn right (e.g. 10, to turn 10 degrees to the right).
You  may want to write this code down in a new file (e.g. `shipexample.py`).

### shipexample.py
~~~~
class Ship():
    def __init__(self,x,y,v,dir):
        self.x = x
        self.y = y
        self.v = v
        self.dir = dir
    
    def move(self, dt):
        self.x = self.x + self.v*cos(self.dir*DEG_TO_RAD)*dt
        self.y = self.y + self.v*sin(self.dir*DEG_TO_RAD)*dt
        
    def show(self):
        print self.x
        print self.y
        
    def turn(self,deg):
        self.dir = self.dir + deg
~~~~

So, how can we create and use an object?

The following example illustrates the creation of a Ship and the usage of its operations. 
The first line creates a new Ship, with x=10, y=20, v=5, dir=30. What about that self parameter we saw earlier? That one is implicit, and is assigned to the variable ship we are creating. The same will happen with the remaining operations, where self is ship. The second line shows the ship's coordinates, the third moves it 10 times units, and so on. Try it out. Do you understand the output?

~~~~
ship = Ship(10,20,5,30)
ship.show()
ship.move(10)
ship.show()
ship.turn(90)
ship.move(10)
ship.show()
~~~~

## Putting it all together

Let us make sure you did not get lost so far. By now, your file should look like this:

### shipexample.py
~~~~
from math import sin, cos, pi

DEG_TO_RAD = pi / 180

class Ship():
    def __init__(self,x,y,v,dir):
        self.x = x
        self.y = y
        self.v = v
        self.dir = dir

    def move(self, dt):
        self.x = self.x + self.v*cos(self.dir*DEG_TO_RAD)*dt
        self.y = self.y + self.v*sin(self.dir*DEG_TO_RAD)*dt

    def show(self):
        print self.x
        print self.y

    def turn(self,deg):
        self.dir = self.dir + deg

ship = Ship(10,20,5,30)
ship.show()
ship.move(10)
ship.show()
ship.turn(90)
ship.move(10)
ship.show()
~~~~

We can use classes, and objects modelled by them, to create abstractions of real-world concepts. And, of course, you can also use them to model your game concepts. In the Asteroids game, you can think of concepts such as spaceships, asteroids, shots, etc. 

Are you ready to create a game?

# Games

An *Arcade* game is usually built of a scenario (in this example, a 2D scenario) where several objects move around following well specified rules, such as those we were discussing for spaceships. The flow of time is simulated. At each time tick, we ask each of the objects to update their state (e.g. by moving). 

We prepared a base game, which you will find in [https://bitbucket.org/visitas_di_fct_unl/vector_arcade/](https://bitbucket.org/visitas_di_fct_unl/vector_arcade/). 
This web page has an online repository for this game project. On the left hand side of the image, you will find a link called `Downloads` beside a cloud shaped icon. 
Press the icon, and download the repository to your computer. Unzip it and open the unzipped folder with Brackets. All done?
Please download this very basic version of the game. Then, we will study its code and extend it to make an awesome game. So much better than the original. You will see. Better yet, you will do it yourself! Here or later, at home.

## The Asteroids game

As a starting point, we created a basic version of the Asteroids game. You will find several assets in this repository, including source code files (with the extension `.py`) a `Readme.md` file (the one you are reading right now) and a few folders (models, textures and other resources) with resources for the visuals of the game you are creating. This basic version was created by adapting one of the several examples available in the Panda3D platform. This code implements the original version of the game, and can be readily executed with the `python` interpreter which is installed in the lab computers. You have been using the interpreter to try out all the previous examples, right?

The code is relatively complex but well documented and easy to read. With the exception of the file `main.py`, each source code file implements one of the classes we will use to program our Asteroids game. Let us go through these files to learn the basic concepts.

### main.py

The main file of this program is called... `main.py` :-)
This is the only source code file in our repository which does not correspond to a particular class. It is used as an entry point for our program. It creates a game of type `AsteroidsGame` and runs it. And that is all it does.

~~~~
from asteroidsgame import AsteroidsGame

# Let us create an object from our AsteroidsDemo class
demo = AsteroidsGame()
# and ask it to run
demo.run()
~~~~

### asteroidsgame.py

This file contains the game itself. To be more precise, it contains a class called `AsteroidsGame` which implements the game.
The class declaration is associated with another class called `Game2D` which hides some complex details of the Panda3D *game engine* and takes care of making this a 2D game. In the constructor operation `__init__`, we initialize the `Game2D` on top of which the `AsteroidsGame` class is built. We make several initializations here, but for now we should focus on a particular one: the creation of the actors in our game (the `self.createActors` command).

~~~~
class AsteroidsGame(Game2D):
    def __init__(self):
        Game2D.__init__(self)
        ...
        self.createActors()
        ...
~~~~

So, what are these actors? In a game, the actors are the elements which participate in the game. In this case, we will have a spaceship, asteroids and bullets. We create actors with the `createActors` operation. This operation creates one spaceship (with the `addShip` operation) and several asteroids (with the `spawnAsteriods` operation).

~~~~
    def createActors(self):
        self.addShip(Ship())
        self.spawnAsteroids()
~~~~

As we discussed earlier, the flow of time is simulated in the game. At each time tick, we will ask each of the actors to update its state. 
This will imply updating the ship, asteroids and shots, handling user commands, detect collisions (e.g. of the spaceship with an asteroid), etc. 
The `gameloop` operation handles all these things for a single time tick. In other words, it updates the whole state of the game. In each iteration of the game, 
this `gameLoop` operation is executed exactly once. In the end of all these things, the operation returns Game2D.cont, to keep the game loop running. 
So, in a nutshell, the game is performed by repeating the execution of this `gameLoop` operation. This is somewhat similar to what we did earlier with the `move` operation of our spaceship.

~~~~
def gameLoop(self, task, dt):
    ...
    return Game2D.cont
~~~~

## Actors

We have several classes representing the actors in our game, namely `Ship`, `Asteroid`, and `Bullet`. Each of them is in a corresponding file (`ship.py`, `asteroid.py`, and `bullet.py`, respectively). Let us have a look at the declaration of each of these classes.

### ship.py
~~~~
class Ship(GameActor):
    def __init__(self):
        GameActor.__init__(self, SHIP_FILENAME, LPoint2(400,300), LVector2(16,16))
        ...
~~~~

### asteroid.py
~~~~
class Asteroid(GameActor):
    def __init__(self, pos=LPoint2(0,0)):
        GameActor.__init__(self, choice(["asteroid1.png","asteroid2.png","asteroid3.png"]), pos, LVector2(60,60))
        ...
~~~~

### bullet.py
~~~~
class Bullet(GameActor):
    def __init__(self, pos=LPoint2(0,0)):
        GameActor.__init__(self, BULLET_FILENAME, pos, LVector2(4,4))
        ...
~~~~

As you probably noticed, all these classes are related to the `GameActor` class. This `GameActor` class plays the role of a super-class, from which the several actors in our game are derived. This is useful, as it allows us to specify behavior which is common to all actors in our game in the `GameActor` class. The full specification of `GameActor` is available in the file `gameactor.py`, but we do not need to go in there right now.

There is a lot more to explore, but let's go step by step.

# Challenges

We have prepared some challenges for you. Are you ready?

## Change the looks of the Asteroids game

This starting version was built to look like the classic Asteroids game in the early 80's. To be fair, its looks are kind of outdated. So, perhaps we could start by improving them. The way the different actors look is controlled by the images we set up as resources for our game. So, if we change those images from those old-fashioned black and white drawings to beautiful high definition pictures, the game will look much better. In terms of programming, all we need to do is change the files associated with each actor. So, let us go back to our actors. Each of them has an associated picture, right?

### asteroidsgame.py

Well, this one is not really an actor, right. As you remember, `AsteroidsGame` is a class representing our game. As it turns out, this class also has an associated image, representing the background upon which our `Ship`, `Asteroid`s and `Bullet`s are going to be rendered. We should update it. So, let us ditch the ugly

~~~~
BACKGROUND_FILENAME = "stars.jpg"
~~~~

and replace it by the gorgeous

~~~~
BACKGROUND_FILENAME = "sparkling-stars.jpg"
~~~~

Much better, right?

### asteroid.py

We have to do something similar for the asteroids. Instead of the old asteroids pictures, change from 

~~~~
GameActor.__init__(self, choice(["asteroid1.png","asteroid2.png","asteroid3.png"]), pos, LVector2(60,60))
~~~~

to

~~~~
GameActor.__init__(self, choice(["asteroid-1.png","asteroid-2.png","asteroid-3.png"]), pos, LVector2(60,60))
~~~~

By the way choice() is a python predefined is radomly one of the options, in this case one of the asteroid images.


### ship.py

The "old school" image is stored in a file called `ship.png`. But we can use a new image.
Actually, we are goint to do more than that. We are going to define two alternative images for the ship. 
One with the thrust, and one without it. Lookup the current definition for the `SHIP_FILENAME`

~~~~
SHIP_FILENAME = "ship.png"
~~~~

and change it so that we have two versions:

~~~~
SHIP_FILENAME = "ship-no-thrust.png"
SHIP_FILENAME_THRUST = "ship-with-thrust.png"
~~~~

Then, in the `__init__` operation of the `Ship` class, we need to add the following commands to the end of this operation:

~~~~
self.tex_normal = loader.loadTexture("textures/" + SHIP_FILENAME)
self.tex_thrust = loader.loadTexture("textures/" + SHIP_FILENAME_THRUST)
~~~~

Now, we have two alternative textures for the ship. We have to choose which one is active, at each time. Let us add a new operation to the Ship class, called `showThrust`, which you can locate in the end of the class.

~~~~
    def showThrust(self, yes):
        if yes:
            self.setTexture(self.tex_thrust,1)
        else:
            self.setTexture(self.tex_normal,1)
~~~~

## Modify the behavior of actors

The  next challenge is to modify the behavior of one of the actors. In this case, we will modify the ship's behavior. We want to be able to see when the ship is accelerating. So, whenever the player presses the accelerate button, *up arrow key*  the image should change to show the exhaustion jet of the ship.

### asteroidsgame.py

Now that we have this new `thrust` operation in the `Ship` class, we may use it when the player presses the accelerate key, which is the *up arrow key*. If the player is pressing the accelerate key, we update the velocity and enable the thrust to true, with the command `self.showThrust(True)`. Otherwise (the `else` part of this decision) we set the thrust to false, with the command `self.showThrust(False)`.
We should do this at every run of the game loop. So, in the `AsteroidsGame` class we need to tweak a bit what happens when that key is pressed (and, conversely, when the key is *not* pressed). 
Look for the test `if self.keys["accel1"]:` It is currently just calling the `thrust` operation. Add the animation effect there.
The code should look like this, in the end:

~~~~
    def gameLoop(self, task, dt):
        ...
            if self.keys["accel1"]:
                self.getShip(PLAYER_ONE).thrust(dt)  
                self.getShip(PLAYER_ONE).showThrust(True)
            else:
                self.getShip(PLAYER_ONE).showThrust(False)
            ...
~~~~

Now the ship should be able to accelerate with great style.

#### Enable laser cannon.

The guy who programmed this ship was probably in a rush. He forgot to turn on the cannon. What a nightmare. How can this ship survive the asteroids, without a cannon? This cannot be! Your mission is to enable the cannon (through its corresponding command). Can you figure out what to do?

~~~~
    def gameLoop(self, task, dt):
        ...
            if self.keys["fire1"]:
                if self.getShip(PLAYER_ONE).canFire():
                    self.getShip(PLAYER_ONE).fire(self)
        ...
~~~~

#### Rotating the ship

Now that we are able to accelarate the ship, let's tell the ship to rotate everytime we detect that the left or right arrow keys are down.
We need to do this in our AsteroidsGame.gameLoop() method:

~~~~
    def gameLoop(self, task, dt):        
    ...
        if self.keys["turnRight1"]:
            self.getShip(PLAYER_ONE).turnRight(dt)
        elif self.keys["turnLeft1"]:
            self.getShip(PLAYER_ONE).turnLeft(dt)
    ...
~~~~

An interesting fact in the code above is that we are instructing the ship to rotate (left or right) by an ammount that is dependent on the elapsed time from the last frame. The idea is to keep a constant rotation rate. If the computer takes more time between two consecutive steps the value of dt will be higher and so will the rotation.
Now we need to change the Ship class to handle the new operations. 
Rotating right and rotating left should be pretty similar. In fact on can be implemented using the other by using a negative angle: turing left is like turning right a negative value. We opted to implement a turn() operation and have both turnLeft() and turnRight() use that operation:

~~~~
    def turn(self, amount):
        heading = self.getR()
        heading = heading + amount
        self.setR(heading)
    
    def turnLeft(self, dt):
        self.turn(-dt*TURN_RATE)

    def turnRight(self, dt):
        self.turn(dt*TURN_RATE)
~~~~

Note that we have multiplied the elapsed time by a constant (Ship.ROTATION_SPEED) to fine tune the angle that the ship rotates per unit time.
The Ship.ROTATION_SPEED constant is declared inside the Ship class, but outside any method:

~~~~
class Ship(GameActor):
    ...
    ROTATION_SPEED = 200
    ...
~~~~

It's time to see the ship spinning in space. Run the program!

## Extend the game wiht a PowerUp feature

By this time, you already understand how all this works. We now challenge you to place somewhere in space a power up, for the ship to collect. This will make your ship much more powerful. Instead of one shot straight ahead, the ship will be able to fire 5 shots simultaneously, in several directions. Can you make it? This will require a bit more work than the previous ones. Go ahead and try it. Do not be shy to ask for help from the activity monitors, if necessary. (Hint: we added an image in the repository which you may want to use).

## Use your imagination

You did see that we had several asteroids built from the same class. Why not do the same for ships? For example, you could have two ships, either cooperating to survive the asteroids, or even competing in an epic battle, while dodging the asteroids.

# Referências

[Panda 3D documentation - Asteroids example](https://www.panda3d.org)
