from panda3d.core import LVector2, LPoint2
from gameactor import GameActor
from bullet import Bullet
from math import sin, cos, pi

SHIP_FILENAME = "ship.png"
BULLET_SPEED = 200          # Speed bullets move
BULLET_REPEAT = .2          # How often bullets can be fired
TURN_RATE = 360             # Degrees ship can turn in 1 second
ACCELERATION = 200          # Ship acceleration in units/sec/sec
MAX_VEL = 120               # Maximum ship velocity in units/sec
MAX_VEL_SQ = MAX_VEL ** 2   # Square of the ship velocity
DEG_TO_RAD = pi / 180       # translates degrees to radians for sin and cos

class Ship(GameActor):
    def __init__(self):
        GameActor.__init__(self, SHIP_FILENAME, LPoint2(400,300), LVector2(16,16))
        self.nextBullet = 0
        self.lazerSound = loader.loadSfx("sounds/Lazer.aif")
        self.enginesSound = loader.loadSfx("sounds/Engines.aif")

    def canFire(self):
        # check to see if the ship can fire
        return self.time > self.nextBullet

    # This method will create bullet and return it
    def fire(self, game):
        # Do the actual firing
        bullet = Bullet()
        bullet.setPos(self.getPos())
        # Bullets are fired with a velocity vector combined from
        # the ship's velocity and the ship's heading
        direction = DEG_TO_RAD * self.getR()
        vel = self.getVelocity() + LVector2(sin(direction), cos(direction)) * BULLET_SPEED
        bullet.setVelocity(vel)
        self.lazerSound.play()

        # And disable firing for a bit
        self.nextBullet = self.time + BULLET_REPEAT
        game.addBullet(bullet)

    def turn(self, ammount):
        heading = self.getR()
        heading += ammount
        self.setR(heading % 360)

    # Rotates ship to the left
    def turnLeft(self, dt):
        self.turn(-dt*TURN_RATE)

    # Rotates ship to the right
    def turnRight(self, dt):
        self.turn(dt*TURN_RATE)

    # Add thrust to the ship
    def accelerate(self, dt):
        # Thrust causes acceleration in the direction the ship is currently
        # facing
        heading_rad = DEG_TO_RAD * self.getR()
        # This builds a new velocity vector and adds it to the current one
        # relative to the camera, the screen in Panda is the XZ plane.
        # Therefore all of our Y values in our velocities are 0 to signify
        # no change in that direction.
        newVel = LVector2(sin(heading_rad), cos(heading_rad)) * ACCELERATION * dt

        newVel += self.getVelocity()
        # Clamps the new velocity to the maximum speed. lengthSquared() is
        # used again since it is faster than length()
        if newVel.lengthSquared() > MAX_VEL_SQ:
            newVel.normalize()
            newVel *= MAX_VEL
        self.setVelocity(newVel)
        self.enginesSound.play()

