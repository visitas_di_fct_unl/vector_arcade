from random import randint, random
from math import sin, cos, pi

from panda3d.core import LPoint2, LVector2
from gameactor import GameActor

ASTEROID_FILENAME = "asteroid%d.png"
AST_MIN_SCALE = 30  # If and asteroid is smaller than this and is hit, it disappears
AST_VEL_SCALE = 2.2  # How much asteroid speed multiplies when broken up
AST_SIZE_SCALE = .6  # How much asteroid scale changes when broken up
AST_INIT_VEL = 20    # Velocity of the largest asteroids

class Asteroid(GameActor):
    def __init__(self, pos=LPoint2(0,0)):
        GameActor.__init__(self, ASTEROID_FILENAME % (randint(1,3)), pos, LVector2(60,60))
        # Heading is a random angle in radians
        heading = random() * 2 * pi

        # Converts the heading to a vector and multiplies it by speed to
        # get a velocity vector
        v = LVector2(sin(heading), cos(heading)) * AST_INIT_VEL
        self.setVelocity(v)
        self.dieSound = loader.loadSfx("sounds/Die.aif")
        self.die2Sound = loader.loadSfx("sounds/Die 2.aif")

    def handleHit(self, game):

        # Check if the asteroid is big enough and if so, create two new ones
        if self.getScale().getX() > AST_MIN_SCALE:

            # create a velocity vector perpendicular to the original one
            vel = LPoint2(self.getVelocity().getY(), -self.getVelocity().getX())

            # scale up its velocity (smaller fragments move faster)
            vel *= AST_VEL_SCALE

            # scale down its size
            scale = self.getScale() * AST_SIZE_SCALE

            # Create a first asteroid
            newAsteroid = Asteroid(self.getPosition())
            newAsteroid.setVelocity(vel)
            newAsteroid.setScale(scale)
            game.addActor("asteroids", newAsteroid)

            # Create a second asteroid
            newAsteroid = Asteroid(self.getPosition())
            newAsteroid.setVelocity(vel * -1)
            newAsteroid.setScale(scale)
            game.addActor("asteroids", newAsteroid)
            
            self.dieSound.play()
        else:
            self.die2Sound.play()

        # Destroy the original asteroid
        self.kill()
