import random

from panda3d.core import LPoint2, LVector2
from gameactor import GameActor

BULLET_FILENAME = "bullet.png"
BULLET_LIFE = 2             # How long bullets stay on screen before removed

class Bullet(GameActor):
    def __init__(self, pos=LPoint2(0,0)):
        GameActor.__init__(self, BULLET_FILENAME, pos, LVector2(4,4))
        # Set the bullet expiration time to be a certain amount past the
        # current time
        self.setExpires(BULLET_LIFE)

