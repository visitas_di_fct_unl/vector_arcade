# This Asteroids game is based on an original demo distributed with
# the Panda3D game engine software.
# It was adapted by Fernando Birra and Joao Seco to illustrate
# topics from Computer Science to young students.
#
# Original Authors: Shao Zhang, Phil Saltzman, and Greg Lindley
# Last update: 2016-03-04

import sys
from math import sin, cos, pi
from random import randint, choice, random

from panda3d.core import LPoint2, LVector2
from direct.interval.MetaInterval import Sequence
from direct.interval.FunctionInterval import Wait, Func

from game2d import Game2D
from ship import Ship
from asteroid import Asteroid


# Constants that will control the behavior of the game. It is good to group constants
# like this so that they can be changed once without having to find everywhere they
# are used in code

BACKGROUND_FILENAME = "stars.jpg"

SAFE_MARGIN = 80    # Margin around the center of the screen where no asteroids will appear

# This class represents the whole game. Our asteroids game will
# a particular instance of this class.

PLAYER_ONE = 0
PLAYER_TWO = 1

ASTEROIDS_PER_LEVEL = 10    # Number of asteroids on start

class AsteroidsGame(Game2D):
    def __init__(self):
        Game2D.__init__(self)
        self.setBackgroundColor((0,0,0,1))
        self.setBackground(BACKGROUND_FILENAME)

        self.shipsNeeded = True
        self.createActors()


        # A dictionary of what keys are currently being pressed
        # The key events update this list, and our task will query it as input
        self.keys = {"turnLeft1": 0, "turnRight1": 0,
                     "accel1": 0, "fire1": 0}

        self.accept("escape", sys.exit)  # Escape quits
        # Other keys events set the appropriate value in our key dictionary
        self.accept("arrow_left",     self.setKey, ["turnLeft1", 1])
        self.accept("arrow_left-up",  self.setKey, ["turnLeft1", 0])
        self.accept("arrow_right",    self.setKey, ["turnRight1", 1])
        self.accept("arrow_right-up", self.setKey, ["turnRight1", 0])
        self.accept("arrow_up",       self.setKey, ["accel1", 1])
        self.accept("arrow_up-up",    self.setKey, ["accel1", 0])
        self.accept("space",          self.setKey, ["fire1", 1])


    def setKey(self, key, val):
        self.keys[key] = val

    # This is our game loop, where all the frame by frame logic will happen.
    # We need to return self.cont to keep the game loop running
    # This gets called after the base class Game2D has done all its general gameloop activties
    def gameLoop(self, task, dt):

        ships = self.getActorsFromBucket("ships")

        # If there is at least one ship (PLAYER_ONE), control it...
        if ships != None and len(ships)>=1:
            # check the key controls and instruct the ship to do what is necessary
            if self.keys["turnRight1"]:
                self.getShip(PLAYER_ONE).turnRight(dt)
            elif self.keys["turnLeft1"]:
                self.getShip(PLAYER_ONE).turnLeft(dt)
            if self.keys["accel1"]:
                self.getShip(PLAYER_ONE).accelerate(dt)
            if self.keys["fire1"]:
                if self.getShip(PLAYER_ONE).canFire():
                    self.getShip(PLAYER_ONE).fire(self)
                # Remove the fire flag until the next spacebar press
                self.keys["fire1"] = 0

        # check for collisions between bullets and asteroids
        bullets = self.getActorsFromBucket("bullets")
        asteroids = self.getActorsFromBucket("asteroids")

        # Only check for collisions between bullets and asteroids if they both exist
        if bullets != None and asteroids != None:
            for b in bullets:
                for asteroid in asteroids:
                    if b.collidesWith(asteroid):
                        asteroid.handleHit(self)
                        b.kill()

        # check for collisions between the ship(s) and asteroids

        # Only check for collisions between ship(s) and asteroids if they both exist
        if ships != None and asteroids != None:
            for s in ships:
                for asteroid in asteroids:
                    if s.collidesWith(asteroid):
                        s.kill()

        # check if the object has run out of the screen boundaries. If so, wrap around...
        for key in self.actors.keys():
            for obj in self.actors[key]:
                # check if the object has run out of the screen boundaries. If so, wrap around...
                xPos = newXPos = obj.getPosition().getX()
                yPos = newYPos = obj.getPosition().getY()
                if xPos > self.width:
                    newXPos = xPos - self.width
                elif xPos < 0:
                    newXPos = xPos + self.width
                if yPos > self.height:
                    newYPos = yPos - self.height
                elif yPos < 0:
                    newYPos = yPos + self.height
                obj.setPosition(LPoint2(newXPos, newYPos))

        # If there are no more asteroids, respawn them.
        if len(asteroids) == 0:
            self.spawnAsteroids()

        # Test if there are no more ships in action
        if len(ships) == 0:
            if self.shipsNeeded:
                # time to relaunch the ships.
                Sequence(Wait(2),Func(self.spawnShips)).start()
                self.shipsNeeded = False
        else:
            self.shipsNeeded = True

        return Game2D.cont

    def getSafeRandomPos(self):
        x = choice(tuple(range(0,self.width/2-SAFE_MARGIN)) + tuple(range(self.width/2+SAFE_MARGIN, self.width)))
        y = choice(tuple(range(0,self.height/2-SAFE_MARGIN)) + tuple(range(self.height/2+SAFE_MARGIN, self.height)))
        return LPoint2(x,y)

    def createActors(self):
        self.spawnShips()
        self.spawnAsteroids()

    def spawnShips(self):
        self.addShip(Ship())

    def spawnAsteroids(self):
        for i in range(0,ASTEROIDS_PER_LEVEL):
            asteroid = Asteroid()
            asteroid.setPosition(self.getSafeRandomPos())
            self.addAsteroid(asteroid)

    def getShip(self, index):
        return self.getActorsFromBucket("ships")[index-1]

    def addShip(self, ship):
        self.addActor("ships", ship)

    def addBullet(self, bullet):
        self.addActor("bullets", bullet)

    def addAsteroid(self, asteroid):
        self.addActor("asteroids", asteroid)


